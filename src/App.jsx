import "./App.css";
import Temperature from "./components/Temperature";

function App() {
  return (
    <div className="bg-grey-900">
      <Temperature />
    </div>
  );
}

export default App;
