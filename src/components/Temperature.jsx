import React, { useState, useEffect } from "react";
import axios from "axios";

const Temperature = () => {
  const [Resultat, setResultat] = useState("0");
  const [temp, setTemp] = useState();
  const [products, setProducts] = useState([]);

  const handleConversion = (e) => {
    e.preventDefault();
    const f = parseFloat(temp) * 1.8 + 32;
    setResultat(f);
  };

  const handleReset = (e) => {
    e.preventDefault();
    setTemp("");
    setResultat("");
  };

  const handelGetproduct = () => {
    axios
      .get("https://dummyjson.com/products")
      .then((res) => {
        console.log(res.data.products);
        setProducts(res.data.products);
      })

      .catch((err) => console.log(err));
  };

  useEffect(() => {
    handelGetproduct();
  }, []);

  return (
    <div>
      <div> Affichage Resultat </div>
      <div> {Resultat} </div>
      <form className="flex flex-col">
        <label htmlFor="">temperature</label>
        <input
          onChange={(e) => setTemp(e.target.value)}
          type="text"
          value={temp}
          className="px-2 border-2 border-blue-700"
        />
        <button
          onClick={handleConversion}
          className="bg-blue-700 text-white p-2"
        >
          Convertir
        </button>
        <button
          type="reset"
          onClick={handleReset}
          className="text-white p-2 bg-slate-500"
        >
          Reset
        </button>
      </form>
      <div className="my-5 bg-orange-300">
        <h1>Listing Products</h1>
        </div>

        <div className="flex ">
          {products.map((v) => (<div> {v.id}</div>  ))}
        
        
          {products.map((v) => (<div> {v.title}</div>))}
        
          {products.map((v) => (<div> {v.description}</div>))}
        </div>























      
    </div>
  );
};

export default Temperature;
